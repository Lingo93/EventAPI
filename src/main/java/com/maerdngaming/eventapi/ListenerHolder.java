package com.maerdngaming.eventapi;

import java.lang.reflect.Method;

class ListenerHolder {

	private final EventListener listener;
	private final Method eventMethod;
	private final boolean ignoreProcessed;
	
	public ListenerHolder(EventListener listener, Method eventMethod, boolean ignoreProcessed) {
		this.listener = listener;
		this.eventMethod = eventMethod;
		this.ignoreProcessed = ignoreProcessed;
	}
	
	public Method getEventMethod() {
		return eventMethod;
	}
	
	public EventListener getListener() {
		return listener;
	}
	
	public boolean isIgnoreProcessed() {
		return ignoreProcessed;
	}
}
