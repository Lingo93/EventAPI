package com.maerdngaming.eventapi;

public interface ExceptionHandler {

	/**
	 * 
	 * @param e
	 * @return true to cancel the event processing
	 */
	public boolean handleException(Exception e);
}
