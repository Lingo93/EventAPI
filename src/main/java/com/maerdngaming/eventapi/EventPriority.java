package com.maerdngaming.eventapi;

import java.util.Arrays;
import java.util.Comparator;

public enum EventPriority {

		LOWEST(10),
		LOWER(20),
		LOW(30),
		NORMAL(40),
		HIGH(50),
		HIGHER(60),
		HIGHEST(70);
	
	public static class EventPriorityComperator implements Comparator<EventPriority> {
		@Override
		public int compare(EventPriority o1, EventPriority o2) {
			return Integer.compare(o1.priority, o2.priority);
		}
	}
	
	public static final EventPriorityComperator EVENT_PRIO_COMP = new EventPriorityComperator();
	public static final EventPriority[] SORTED_PRIO;
	static {
		EventPriority[] eps = EventPriority.values();
		Arrays.sort(eps, EventPriority.EVENT_PRIO_COMP);
		SORTED_PRIO = eps;
	}
	
	private final int priority;
	
	private EventPriority(int prio) {
		this.priority = prio;
	}
	
	public int getPriority() {
		return priority;
	}
	
}
