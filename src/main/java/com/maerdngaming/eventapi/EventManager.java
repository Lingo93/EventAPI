package com.maerdngaming.eventapi;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Executor;

public class EventManager {

	private Map<Class<? extends Event>, Map<EventPriority, Set<ListenerHolder>>> listeners = new HashMap<>();
	private ExceptionHandler exceptionHandler = null;
	private final Executor eventExecutor;

	public EventManager() {
		this(null);
	}

	public EventManager(Executor exec) {
		this.eventExecutor = exec == null ? new SelfExecutor() : exec;
	}

	@SuppressWarnings("unchecked") // is checked
	public void registerListener(EventListener listener) {
		synchronized (this.listeners) {
			Method[] methods = listener.getClass().getMethods();
			for (Method m : methods) {
				if (!m.isAnnotationPresent(EventHandler.class))
					continue;
				if (m.getParameterTypes().length != 1)
					continue;
				Class<?> eventClass = m.getParameterTypes()[0].getClass();
				if (!Event.class.isAssignableFrom(eventClass))
					continue;
				EventHandler eh = m.getAnnotation(EventHandler.class);
				Map<EventPriority, Set<ListenerHolder>> priomap = this.listeners.containsKey(eventClass) ? this.listeners.get(eventClass) : null;
				if (priomap == null) {
					priomap = new HashMap<>();
					this.listeners.put((Class<? extends Event>) eventClass, priomap);
				}
				synchronized (priomap) {
					Set<ListenerHolder> holderSet = priomap.containsKey(eh.priority()) ? priomap.get(eh.priority()) : null;
					synchronized (holderSet) {
						if (holderSet == null) {
							holderSet = new HashSet<>();
							priomap.put(eh.priority(), holderSet);
						}
						holderSet.add(new ListenerHolder(listener, m, eh.ignoreIfProcessed()));
					}
				}
			}
		}
	}

	public void callEvent(final Event e) {
		this.eventExecutor.execute(new Runnable() {
			@Override
			public void run() {
				Map<EventPriority, Set<ListenerHolder>> listeners2;
				synchronized (EventManager.this.listeners) {
					listeners2 = EventManager.this.listeners.get(e.getClass());
				}
				for (EventPriority ep : EventPriority.SORTED_PRIO) {
					Set<ListenerHolder> holders;
					synchronized (listeners2) {
						holders = listeners2.get(ep);
					}
					synchronized (holders) {
						for (ListenerHolder lh : holders) {
							try {
								if(lh.isIgnoreProcessed() || !e.isProcessed())
									lh.getEventMethod().invoke(lh.getListener(), e);
							} catch (Exception e1) {
								synchronized (EventManager.this.exceptionHandler) {
									if (EventManager.this.exceptionHandler != null)
										if (EventManager.this.exceptionHandler.handleException(e1))
											return;
								}
							}
						}
					}
				}
			}
		});
	}
	
	@SuppressWarnings("unlikely-arg-type")
	public void unregisterListener(final EventListener listener) {
		synchronized (this.listeners) {
			for(Entry<Class<? extends Event>, Map<EventPriority, Set<ListenerHolder>>> entry : this.listeners.entrySet()) {
				synchronized (entry.getValue()) {
					for(Entry<EventPriority, Set<ListenerHolder>> entry2 : entry.getValue().entrySet()) {
						synchronized (entry2.getValue()) {
							synchronized (entry2.getValue()) {
								entry2.getValue().remove(listener);
							}
						}
					}
				}
			}
		}
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	private class SelfExecutor implements Executor {

		@Override
		public void execute(Runnable command) {
			command.run();
		}
		
	}
}
