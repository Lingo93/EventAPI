package com.maerdngaming.eventapi;

public abstract class AbstractEvent implements Event {

	private boolean proccessed = false;
	
	@Override
	public final boolean isProcessed() {
		return this.proccessed;
	}
	
	@Override
	public final void setProcessed(boolean proccessed) {
		this.proccessed = proccessed;
	}
}
