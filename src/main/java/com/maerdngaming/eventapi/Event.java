package com.maerdngaming.eventapi;

public interface Event {

	public void setProcessed(boolean proccessed);
	public boolean isProcessed();
}
